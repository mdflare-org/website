let blocks = document.querySelectorAll("[data-copy]");

blocks.forEach(block => {

  	// only add button if browser supports Clipboard API
	if (navigator.clipboard) {

		let button = document.createElement("button");
		button.classList.add("copy-button");

		button.innerHTML = `Copy`;

		block.appendChild(button);

		button.addEventListener("click", async () => {

			await copyCode(block);

		});

	}

});

async function copyCode(block) {
	let code = block.querySelector("code");
	let text = code.innerText;

	await navigator.clipboard.writeText(text);

	// get button reference
	let button = block.querySelector("button");

	button.innerHTML = `Copied`;

	setTimeout(() => {
		button.innerHTML = `Copy`;
	}, 1000);

}